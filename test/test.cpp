
#include <iostream>

#include <bg2model/json/parser.hpp>
#include <bg2model/bg2_reader.hpp>

int main(int argc, char ** argv) {
	using namespace bg2model;

	std::string testJson = "{ \"key\":\"value\", \"color\":[0.1,0.2,0.4,1.0] }";
	try {
		json::Value * jsonDoc = json::Parser::ParseString(testJson,true);
		std::string stringValue = json::Value::String((*jsonDoc)["key"],"error");
		std::cout << "Value: " << stringValue << std::endl;
		Vec4 color = json::Value::Vector4((*jsonDoc)["color"]);
		std::cout << "Color: r="
			<< color.comp.x << ", g="
			<< color.comp.y << ", b="
			<< color.comp.z << ", a="
			<< color.comp.w << std::endl;
		delete jsonDoc;
	}
	catch (std::runtime_error err) {
		std::cerr << err.what() << std::endl;
	}
	
	std::string filePath = "test.bg2";
	Bg2Reader reader;
	reader.version([&](uint8_t major, uint8_t minor, uint8_t rev) {
			std::cout << "File version: " <<
				static_cast<int>(major) << "." <<
				static_cast<int>(minor) << "." <<
				static_cast<int>(rev) << std::endl;
		})
	
		.metadata([&](const Bg2Reader::FileMetadata & md) {
			std::cout << "Number of poly list: " << md.numberOfPolyList << std::endl;
		})
	
		.materials([&](const std::string & matString) {
			try {
				json::Value * matData = json::Parser::ParseString(matString);
				matData->eachItem([&](json::Value * val) {
					std::string name = json::Value::String((*val)["name"]);
					float dx = json::Value::Float((*val)["diffuseR"]);
					float dy = json::Value::Float((*val)["diffuseG"]);
					float dz = json::Value::Float((*val)["diffuseB"]);
					float dw = json::Value::Float((*val)["diffuseA"]);
					std::cout << "Color: R=" << dx <<
								 ", G=" << dy <<
								 ", B=" << dz <<
								 ", A=" << dw << std::endl;
					std::string texture = json::Value::String((*val)["texture"]);
					std::cout << "Texture: " << texture << std::endl;
				});
				
				delete matData;
			}
			catch (std::runtime_error & e) {
				std::cerr << "Error parsing material string" << std::endl;
			}
		})
	
		.materialOverrides([&](const std::string & matString) {
			try {
				json::Value * matData = json::Parser::ParseString(matString);
				matData->eachItem([&](json::Value * val) {
					std::string name = json::Value::String((*val)["name"]);
					std::cout << "Material override: " << name << std::endl;
				});
			}
			catch (std::runtime_error & e) {
				std::cerr << "Error parsing material overrides string" << std::endl;
			}
		})
	
		.inJoint([&](float offset[3], float roll, float pitch, float yaw) {
		})
	
		.outJoint([&](float offset[3], float roll, float pitch, float yaw) {
		})
	
		.plistName([&](const std::string & plist) {
			std::cout << "PolyList Found. Name: " << plist << std::endl;
		})
	
		.materialName([&](const std::string & name) {
			std::cout << "Material name: " << name << std::endl;
		})
	
		.vertex([&](const std::vector<float> & v) {
			std::cout << v.size() / 3 << " vertices" << std::endl;
		})
	
		.normal([&](const std::vector<float> & v) {
			std::cout << v.size() / 3 << " normals" << std::endl;
		})
	
		.uv0([&](const std::vector<float> & v) {
			std::cout << v.size() / 2 << " uv0's" << std::endl;
		})
	
		.uv1([&](const std::vector<float> & v) {
			std::cout << v.size() / 2 << " uv1's" << std::endl;
		})
	
		.uv2([&](const std::vector<float> & v) {
			std::cout << v.size() / 2 << " uv2's" << std::endl;
		})
	
		.index([&](const std::vector<unsigned int> & i) {
			std::cout << i.size() / 3 << " polygons" << std::endl;
		})
	
		.error([&](const std::exception & e) {
			std::cerr << e.what() << std::endl;
		});
	
	reader.load(filePath);

	return 0;
}
